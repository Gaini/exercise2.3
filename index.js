const express = require("express");
const bodyParser = require("body-parser");
const app = express();

app.use(bodyParser.urlencoded({extended: true}));
app.use(bodyParser.json());

app.use('/post', function (req, res, next) {
    if (req.header('Header') === undefined) {
        res.sendStatus(401);
    } else {
        next();
    }
});

app.get('/', function (req, res) {
    res.status(200).send('Hello, Express.js');
});

app.get('/hello', function (req, res) {
    res.status(200).send('Hello stranger!');
});

app.get('/hello/:name', function (req, res) {
    res.status(200).send('Hello, ' + req.params.name + '!');
});

app.all('/sub/*', function (req, res) {
    var fullUrl = req.protocol + '://' + req.get('host') + req.originalUrl;
    res.send('You requested URI: ' + fullUrl);
});

app.post('/post', function (req, res) {
    if (Object.keys(req.body).length === 0) {
        res.sendStatus(404);
    } else {
        res.status(200).json(req.body);
    }
});

app.listen(3000);